﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Vuforia
{
    public class Recognize : MonoBehaviour
    {

        public Transform TextTargetName;
        public Transform TextDescription;
        public Transform PanelDescription;


        void Start()
        {

        }


        void Update()
        {

            StateManager sm = TrackerManager.Instance.GetStateManager();
            IEnumerable<TrackableBehaviour> x = sm.GetActiveTrackableBehaviours();

            foreach (TrackableBehaviour tb in x)
            {
                string name = tb.TrackableName;
                ImageTarget im = tb.Trackable as ImageTarget;
                Vector2 size = im.GetSize();


                Debug.Log("Active image target:" + name + " -size:" + size.x); //", " + size.y);

                TextTargetName.GetComponent<Text>().text = name;
                TextDescription.gameObject.SetActive(true);
                PanelDescription.gameObject.SetActive(true);

                if (name == "Brown_Chapel")
                {
                    TextDescription.GetComponent<Text>().text = " The Muskingum Community gathers in the Brown Chapel every thursday durring the 11:00 a.m. " +
                        "common hour. Everyone is welcome and Lunch is provided for students after the service. A link to the religous life is at the bottom. " +

                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "https://www.muskingum.edu/administration/religious-life/worship ";
                }
                if (name == "Montgomery")
                {
                    TextDescription.GetComponent<Text>().text = "Montgomery houses the Enlgish and the Education majors as well as other majors. The building is one of the few on campus without air conditioning. A link to the Education, Special education and English majors is below." +
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "https://www.muskingum.edu/academics/education-earlychildhood " +
                       
                        "https://www.muskingum.edu/academics/education-middle-childhood" +
                       
                        " https://www.muskingum.edu/academics/education-special-education";

                }
                if (name == "Neptune_Art_Studio")
                {
                    TextDescription.GetComponent<Text>().text = "The Neptune Art Studio is home to art majors and art minors. Students take a variety of classes here ranging from painting to ceramics. A link to the art page follows." +
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "https://www.muskingum.edu/academics/art";
                }
                if (name == "Cambridge")
                {
                    TextDescription.GetComponent<Text>().text = "Cambridge is home to many majors including History, Criminal Justice and the plentiful Business majors.  " +
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "https://www.muskingum.edu/academics/business-managment " +
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        " https://www.muskingum.edu/academics/criminal-justice" +
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "https://www.muskingum.edu/academics/history";

                }
                if (name == "John_Glenn_Gymnasium")
                {
                    TextDescription.GetComponent<Text>().text = "The Gymnasium is home to the Athletic Trainers. The Gym is " +
                        "also home to the many sports teams that use the facility such as the Basketball, Volleyball and Lacrosse as well as many more. A link to the Muskingum Athletics page follows as well as a link to the Athletic Traniors page.   " +
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "https://www.muskingum.edu/academics/athletic-training" +
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "http://fightingmuskies.com/landing/index";
                }

            }
        }
    }
}
